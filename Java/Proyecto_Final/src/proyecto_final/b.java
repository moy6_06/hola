package proyecto_final;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;


public class b {
    
    
  public static void main(String[] args) {
 
    Connection con = null;
     PreparedStatement stmt = null;

     String sDriver = "com.mysql.jdbc.Driver";
     String sURL = "jdbc:mysql://localhost:5433/testdb";

     try{
        Class.forName(sDriver).newInstance();    
        con = DriverManager.getConnection(sURL,"postgres","yoyo66");
        
        String sISBN = "84-9815-212-7";
        //String sTitulo = "Yo, Claudio";
        //String sDescripcion="Supuesta autobiografía de Claudio";
        //String sCategoria = "Novela Histórica";
        int idAutor = 3;
                     
        stmt = con.prepareStatement("INSERT INTO libros VALUES (?,?)");//,?,?,?)");
        
        stmt.setString(1,sISBN);
        stmt.setInt(2,idAutor);
        //stmt.setString(3,sTitulo);
        //stmt.setString(4,sDescripcion);
        //stmt.setString(5,sCategoria);
                     
        int retorno = stmt.executeUpdate();
        if (retorno>0)
           System.out.println("Insertado correctamente");      
                     
     } catch (SQLException sqle){
        System.out.println("SQLState: " 
           + sqle.getSQLState());
        System.out.println("SQLErrorCode: " 
           + sqle.getErrorCode());
        sqle.printStackTrace();
     } catch (Exception e){
        e.printStackTrace();
     } finally {
        if (con != null) {
           try{
              stmt.close();
              con.close();
           } catch(Exception e){
              e.printStackTrace();
           }
        }
     }           
   }
    
}
