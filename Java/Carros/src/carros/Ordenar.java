/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package carros;

/**
 *
 * @author moy6_
 */
public class Ordenar extends javax.swing.JInternalFrame {

    /**
     * Creates new form Ordenar
     */
    public Ordenar() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        label1 = new java.awt.Label();
        Ordenar = new java.awt.Button();
        Area = new java.awt.TextArea();
        box = new javax.swing.JComboBox();

        label1.setText("label1");

        setBackground(new java.awt.Color(255, 153, 153));

        Ordenar.setLabel("Ordenar");
        Ordenar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                OrdenarActionPerformed(evt);
            }
        });

        box.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        box.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Marca", "Modelo", "ID" }));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(Area, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addComponent(Ordenar, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(184, 184, 184)
                .addComponent(box, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(62, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(43, 43, 43)
                        .addComponent(box, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addComponent(Ordenar, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 90, Short.MAX_VALUE)
                .addComponent(Area, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        setBounds(25, 25, 527, 380);
    }// </editor-fold>//GEN-END:initComponents

    private void OrdenarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_OrdenarActionPerformed
        Carros car=new Carros();
        int n=box.getSelectedIndex();
        car.brbja(n);
    }//GEN-LAST:event_OrdenarActionPerformed

    public static void motrarC(Constructor [] a){
        String cad="";
        
        for (int i = 0; i < a.length; i++) 
              if(a[i].Marca.equals("")==false )
                cad=cad+"Marca:\t "+a[i].Marca+"\tModelo:\t "+a[i].Modelo+"\tColor:\t"+a[i].Color+"\tID:\t"+a[i].ID+" \n";
        
        Area.setText(cad);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private static java.awt.TextArea Area;
    private java.awt.Button Ordenar;
    private javax.swing.JComboBox box;
    private java.awt.Label label1;
    // End of variables declaration//GEN-END:variables
}
