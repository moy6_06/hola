
package carros;

/**
 *
 * @author moy6_
 */
public class Constructor {
    
    String Marca;
    String Modelo;
    String Color;
    String Placas;
    
    public Constructor() {
        this.Marca = "";
        this.Modelo = "";
        this.Color = "";
        this.Placas = "";
    }
    
    public String getMarca() {
        return Marca;
    }

    public void setMarca(String Marca) {
        this.Marca = Marca;
    }

    public String getModelo() {
        return Modelo;
    }

    public void setModelo(String Modelo) {
        this.Modelo = Modelo;
    }

    public String getColor() {
        return Color;
    }

    public void setColor(String Color) {
        this.Color = Color;
    }

    public String getPlacas() {
        return Placas;
    }

    public void setPlacas(String Placas) {
        this.Placas = Placas;
    }


}
