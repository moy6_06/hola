
package datos;

/**
 *
 * @author moy6_
 */
public class Personas {
    
    
    String nombre;
    String apeido;
    String correo;
    int tel;
    
    public Personas(){
    this.nombre="";
    this.apeido="";
    this.correo="";
    this.tel=0;
    }
    
     public String  getnombre(){
        return nombre;
    }
     
     public void setnombre(String nombre){
         this.nombre=nombre;
       }
     
     public String getapeido(){
         return apeido;
     }
     
     public void setapeido(String apeido){
         this.apeido=apeido;
     }
     
     public String getcorreo(){
         return correo;
     }
     
     public void setcorreo(String correo){
         this.correo=correo;
     }
     
     public void settel(int tel){
        this.tel=tel;    
     }
     
     public int gettel(){
         return tel;
     }
}
